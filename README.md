# Matmatch Frontend Engineer - Coding Exercise


---

There are available 2 simple REST APIs:

`/api/categories`

Returns all available material categoriescontaining the following information:
```
{
  label: str
  id: str
}
```
`/api/materials?categories=<id1>,<id2>,...,<idn>`

Returns the count total number of materials inside the given categories.
```
{ 
  count: int
}
```

Please use the endpoints specified above to implement the following requirements:
 - Display all available material categories, with possibility to select them.
 - Whenever the user selects one or more categories from the list, show the total number of materials available for the given selection.
 - Keep in mind that the `/api/materials` count api is very slow.
 - Implement the UI interface according to the designs linked in the email. 

  Technical Requirements

- Use React JS framework
- Use Redux Store for State management
- Implement tests
 




