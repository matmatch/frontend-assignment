import Head from 'next/head'

export default function Home () {
  return (
    <div>
      <Head>
        <title>Frontend Coding Assignment</title>
        <link rel="icon" href="/favicon.ico"/>
      </Head>

      <main>
        Your solution
      </main>
    </div>
  )
}
