import categories from './_data.json'

function categoriesHandler (req, res) {
  const categoriesList = categories.map(
    ({ id, label }) => ({ id, label })
  )

  res.status(200)
    .json(categoriesList)
}

export default categoriesHandler
