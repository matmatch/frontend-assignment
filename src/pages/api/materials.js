import categories from './_data.json'

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

const sum = (acc, cur) => acc + cur.materialsCount

const delayRange = {
  min: 2,
  max: 4
}

async function materialsHandler (req, res) {
  const ids = req.query.categories || []
  const materialsCount = categories.filter(it => ids.includes(it.id))
    .reduce(sum, 0)

  const randomDelay = Math.floor(Math.random() * (delayRange.max - delayRange.min) * 1000) + delayRange.min * 1000
  console.info(`Sleeping for ${randomDelay}ms`)
  await sleep(randomDelay)
  res.status(200)
    .json({
      materialsCount
    })
}

export default materialsHandler
